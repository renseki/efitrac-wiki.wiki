==========
Intro
==========

Bagian ini akan menjelaskan web service / API yang di support oleh Efitrac,
untuk requestnya ke 

http://SERVER-URL/rest/api 

dengan parameter payload sbb

``Create``
----------
``Request``
```JSON
{
  "uid": "2",
  "pwd": "test1190",
  "data": [
    {
      "transmit_id": "f555f2ae-adb7-4a46-a33f-85d74b2c6b9c",
      "cmd": "execute_kw",
      "method": "create",
      "model": "hotel.country",
      "values": [
        {
          "id": "-1",
          "code": "NK5",
          "name": "North Korea"
        },
        {
          "id": "-2",
          "code": "SDN5",
          "name": "Sudan"
        }
      ]
    }
  ]
}
```
``Reponse Success``
```JSON
{
  "result": [
    {
      "result": [
        {
          "__status": "success",
          "__dummy_id": "-1",
          "id": 17
        }
      ]
    }
  ]
}
```
``Reponse Failed``
```JSON
{
  "result": [
    {
      "result": [
        {
          "__status": "error",
          "__message": "failed to process (com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry 'NK8' for key 'hotel_country_UNIQUE_COUNTRY_CODE'):{\"id\":\"-1\",\"code\":\"NK8\",\"name\":\"North Korea\"}"
        },
        {
          "__status": "error",
          "__message": "failed to process (com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry 'SDN8' for key 'hotel_country_UNIQUE_COUNTRY_CODE'):{\"id\":\"-2\",\"code\":\"SDN8\",\"name\":\"Sudan\"}"
        }
      ]
    }
  ]
}
```
``Search``
----------
``Request``
```JSON
{
  "uid": "2",
  "pwd": "test1190",
  "data": [
    {
      "transmit_id": "4257253e-a2d3-46b1-a832-2d2461d1a4af",
      "cmd": "execute_kw",
      "method": "search",
      "model": "hotel.country",
      "domain": "['|', ('code' ,'=', 'NK'), ('code' ,'=', 'SDN')]",
      "param": {
        "limit": 100
      }
    }
  ]
}
```
``Reponse``
```JSON
{
  "result": [
    {
      "result": [
        13,
        14
      ],
      "transmit_id": "4257253e-a2d3-46b1-a832-2d2461d1a4af",
      "cmd": "execute_kw",
      "method": "search",
      "model": "hotel.country"
    }
  ]
}
```

``Search & Read``
----------
``Request``
```JSON
{
  "uid": "2",
  "pwd": "test1190",
  "data": [
    {
      "transmit_id": "9f0b641c-a54e-482b-a300-602a28ca2b94",
      "cmd": "execute_kw",
      "method": "search_read",
      "model": "hotel.country",
      "domain": "['|', ('code' ,'=', 'NK'), ('code' ,'=', 'SDN')]",
      "param": {
        "limit": 2,
        "fields": [
          "code",
          "name"
        ]
      }
    }
  ]
}
```
``Reponse``
```JSON
{
  "result": [
    {
      "result": [
        {
          "id": 13,
          "write_uid": 1,
          "create_date": "2015-09-08 10:41:14.0",
          "name": "North Korea",
          "create_uid": 1,
          "code": "NK",
          "write_date": "2015-09-08 10:41:14.0"
        },
        {
          "id": 14,
          "write_uid": 1,
          "create_date": "2015-09-08 10:41:15.0",
          "name": "Sudan",
          "create_uid": 1,
          "code": "SDN",
          "write_date": "2015-09-08 10:41:15.0"
        }
      ],
      "transmit_id": "9f0b641c-a54e-482b-a300-602a28ca2b94",
      "cmd": "execute_kw",
      "method": "search_read",
      "model": "hotel.country"
    }
  ]
}
```
``Read``
----------
``Request``
```JSON
{
  "uid": "1",
  "pwd": "test1190",
  "data": [
    {
      "transmit_id": "8a6d99d4-b9bf-4937-a857-32a2f29e4c3c",
      "cmd": "execute_kw",
      "method": "read",
      "model": "hotel.country",
      "ids": [
        1, 2, 3, 4
      ]
    }
  ]
}
```
``Reponse``
```JSON
{
  "result": [
    {
      "result": [
        {
          "id": 1,
          "write_uid": 1,
          "create_date": "2015-09-08 10:39:17.0",
          "name": "Indonesia",
          "create_uid": 1,
          "code": "ID",
          "write_date": "2015-09-08 10:39:17.0"
        },
        {
          "id": 2,
          "write_uid": 1,
          "create_date": "2015-09-08 10:39:17.0",
          "name": "Singapore",
          "create_uid": 1,
          "code": "SG",
          "write_date": "2015-09-08 10:39:17.0"
        },
        {
          "id": 3,
          "write_uid": 1,
          "create_date": "2015-09-08 10:39:17.0",
          "name": "Malaysia",
          "create_uid": 1,
          "code": "MY",
          "write_date": "2015-09-08 10:39:17.0"
        },
        {
          "id": 4,
          "write_uid": 1,
          "create_date": "2015-09-08 10:39:17.0",
          "name": "China",
          "create_uid": 1,
          "code": "CN",
          "write_date": "2015-09-08 10:39:17.0"
        }
      ],
      "transmit_id": "8a6d99d4-b9bf-4937-a857-32a2f29e4c3c",
      "cmd": "execute_kw",
      "method": "read",
      "model": "hotel.country"
    }
  ]
}
```
``Write``
----------
``Request``
```JSON
{
  "uid": "2",
  "pwd": "test1190",
  "data": [
    {
      "transmit_id": "a114375f-2d63-4417-bb00-848d39b5982d",
      "cmd": "execute_kw",
      "method": "write",
      "model": "hotel.country",
      "ids": [
        1
      ],
      "value": {
        "code": "IND1",
        "name": "NKRI"
      }
    }
  ]
}
```
``Reponse``
```JSON
{
  "result": [
    {
      "status": "ok",
      "values": [
        {
          "name": "NKRI",
          "code": "IND1",
          "write_date": "2015-09-08 13:22:50",
          "id": 1
        }
      ],
      "transmit_id": "a114375f-2d63-4417-bb00-848d39b5982d",
      "cmd": "execute_kw",
      "method": "write",
      "model": "hotel.country"
    }
  ]
}
```